/*
 * dab_pwm.h

    Author: strayedelectron
    Created on: 03.02.2023

    DAB Modulation Controller
    Copyright (C) 2023  strayedelectron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef DAB_PWM_H_
#define DAB_PWM_H_

// Some basic defines are in here
#include "dab.h"
// TI 28x driverlib project basics
#include "driverlib.h"
#include "device.h"
#include "board.h"
#include "c2000ware_libraries.h"
// C++ Libs
#include <cmath>

namespace dabmod
{

// Set all PWM DAB Phase shifts
void SetDabPhaseShifts(int16_t phi_dutycycle, uint16_t tau1_dutycycle, uint16_t tau2_dutycycle);
// Set PWM rising edge dead time in nano-seconds
void SetDabDeadTime(uint32_t deadtime_ns);
// Set initial phase shift and dead time to almost zero power transfer
void InitDABsanePhaseDead(void);
// Start/Stop PWM Timers
void TimersEnable(void);
void TimersDisable(void);
// Enable/Disable Bridge Gate drivers
void BridgeOnLatch(void);
void BridgeOffLatch(void);
// Latch status and reset
bool HwErrLatch(void);
void HwLatchReset(void);

// Error handling
void Error(const char *filename, uint32_t line);

}  // namespace dabmod

#endif /* DAB_PWM_H_ */
