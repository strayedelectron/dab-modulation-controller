# DAB Modulation with TI TMS320F280049

## Description
This is a simple project for DAB modulation in laboratory environment.

## License
All my work is under: [![GNU Affero General Public License](https://www.gnu.org/graphics/agplv3-155x51.png)](https://www.gnu.org/licenses/agpl-3.0.de.html)

TI libs are under their own license.
