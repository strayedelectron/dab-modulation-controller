/*
 * dab_pwm.cpp

    Author: strayedelectron
    Created on: 03.02.2023

    DAB Modulation Controller (driverlib)
    Copyright (C) 2023  strayedelectron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

//
// Included Files
//
#include "driverlib.h"
#include "device.h"
#include "board.h"
#include "c2000ware_libraries.h"

// DAB Project includes
#include "dab.h"
#include "dab_led.h"
#include "dab_pwm.h"
#include "dab_ser.h"

//
// Function Prototypes
//
__interrupt void INT_myCPUTIMER0_ISR(void);

//
// Main
//
void main(void)
{

    //
    // Initialize device clock and peripherals
    //
    Device_init();

    //
    // Disable pin locks and enable internal pull-ups.
    //
    Device_initGPIO();

    //
    // Initialize PIE and clear PIE registers. Disables CPU interrupts.
    //
    Interrupt_initModule();

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Assign the interrupt service routines to ePWM interrupts
    //
    // disabled because not used
//    Interrupt_register(INT_EPWM1, &epwm1ISR);
//    Interrupt_register(INT_EPWM2, &epwm2ISR);
//    Interrupt_register(INT_EPWM3, &epwm3ISR);
//    Interrupt_register(INT_EPWM4, &epwm4ISR);

    // Disable sync(Freeze clock to PWM as well). GTBCLKSYNC is applicable
    // only for multiple core devices. Uncomment the below statement if
    // applicable.
    //
    // SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_GTBCLKSYNC);
    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // PinMux and Peripheral Initialization
    //
    Board_init();

    // Turn off Bridge before enabling the PWM Timers
    dabmod::BridgeOffLatch();
    // Set initial phase shift and dead time to almost zero power transfer
    dabmod::InitDABsanePhaseDead();

    //
    // Enable sync and clock to PWM
    //
    // This will enable PWM output
    // DANGER may drive Bridge with random values!
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    //
    // Enable ePWM interrupts
    //
    // disabled because not used
//    Interrupt_enable(INT_EPWM1);
//    Interrupt_enable(INT_EPWM2);
//    Interrupt_enable(INT_EPWM3);
//    Interrupt_enable(INT_EPWM4);

    //
    // C2000Ware Library initialization
    //
    C2000Ware_libraries_init();

    //
    // Enable Global Interrupt (INTM) and real time interrupt (DBGM)
    //
    EINT;
    ERTM;

//    // Some basic sync tests
//    dabmod::SetDabPhaseShifts(0, 5000, 5000);
//    dabmod::SetDabPhaseShifts(0, 5000, 2500);
//    dabmod::SetDabPhaseShifts(0, 2500, 5000);
//    // This would do a negative power transfer
//    dabmod::SetDabPhaseShifts(0, 1250, 2500);
//    // This would do a zero power transfer
//    dabmod::SetDabPhaseShifts(625, 1250, 2500);
//    // This would do a even more negative power transfer
//    dabmod::SetDabPhaseShifts(-625, 1250, 2500);

    //
    // DAB PWM initial configuration
    //

    // Enable Bridge only after setting some sane phase angles
    dabmod::BridgeOnLatch();

    // Enable Serial Port
    dabser::init();
    dabser::SendMessage("\nDAB Testbench at your service!\n");

    // Variables for Loop and Debug
    // DAB PWM dead time for test
    volatile uint16_t deadtime1_ns = 50;
    volatile uint16_t deadtime2_ns = 50;
    // DAB Phase angles/duty cycles test defaults
    volatile int16_t phi_dutycycle = 0;
    volatile uint16_t tau1_dutycycle = 5000;
    volatile uint16_t tau2_dutycycle = 5000;
    // DAB Params all in one struct
    struct dab_params dab;
    // Enable Bridge
    uint16_t dab_enable = 1;
    // DAB State rx from serial
    // 0x00AA: ok, running
    // 0x00EE: error, switch off
    uint16_t dab_state = 0x00AA;
    // Serial Messages
    char rxChar;

    while (1)
    {
        // Enable/Disable DAB in Debug mode
        if (dab_enable)
        {
            dabmod::BridgeOnLatch();
        }
        else
        {
            dabmod::BridgeOffLatch();
        }

        // UPDATE DAB PARAMS EVERY LOOP CYCLE FOR DEBUG
        dabmod::SetDabDeadTime(deadtime1_ns);
        dabmod::SetDabPhaseShifts(phi_dutycycle, tau1_dutycycle, tau2_dutycycle);

        // Check if some HW Error occurred, for DAB mainly useless!
        if (dabmod::HwErrLatch())
        {
            dabled::LED_Error_ON();
        }
        else
        {
            dabled::LED_Error_OFF();
        }

        //
        // Check if a character is available in the receive FIFO.
        //
        if (SCI_getRxFIFOStatus(SerialA_BASE) != SCI_FIFO_RX0)
        {
            if (SCI_getRxFIFOStatus(SerialA_BASE) == SCI_FIFO_RX16)
            {
                // Set warn/distress blink pattern
                CPUTimer_setPeriod(myCPUTIMER0_BASE, 3000);
                dabser::SendMessage("ERR: RX FIFO Buffer overflow!\n");
            }
            rxChar = SCI_readCharNonBlocking(SerialA_BASE);
            // RX byte blink
            dabled::LED_Status_Toggle();
            if (dabser::charbuf_not_full())
            {
                dabser::charbuf_append(rxChar);
            }
            else
            {
                // Set warn/distress blink pattern
                CPUTimer_setPeriod(myCPUTIMER0_BASE, 3000);
                dabser::SendMessage("ERR: RX Char Buffer overflow!\n");
            }
        }
        else
        {
            //
            //If there is any error return
            //
            if ((SCI_getRxStatus(SerialA_BASE) & SCI_RXSTATUS_ERROR) != 0U)
            {
                // Set warn/distress blink pattern
                CPUTimer_setPeriod(myCPUTIMER0_BASE, 3000);
                dabser::SendMessage("ERR: Unknown Serial RX Error!\n");
            }
        }

        //
        // Check if the data packet is complete and parse it.
        //
        if (dabser::handle_data_packet(&dab))
        {
            if (dab.phi < -2500 or dab.phi > 2500 or dab.tau1 > 5000 or dab.tau2 > 5000)
            {
                // Set critical/distress blink pattern
                CPUTimer_setPeriod(myCPUTIMER0_BASE, 1000);
                dabser::SendMessage("ERR: DAB Phases out of range!\n");
            }
            else
            {
                dab_state = dab.state;
                phi_dutycycle = dab.phi;
                tau1_dutycycle = dab.tau1;
                tau2_dutycycle = dab.tau2;
                deadtime1_ns = dab.deadtime1;
                deadtime2_ns = dab.deadtime2;
                // Set default blink pattern, because we have set/reset everything
                CPUTimer_setPeriod(myCPUTIMER0_BASE, 10000);
            }

            // Check for new state and process actions
            switch (dab_state)
            {
            case 0x00AA:
                dab_enable = 1;
                break;
            case 0x00EE:
                dab_enable = 0;
                break;
            }
        }
    } // Main loop cycle end
}

//
// cpuTimer0ISR - Counter for CpuTimer0
//
__interrupt void INT_myCPUTIMER0_ISR(void)
{
    // Blink green LED every second so we can see that everything is fine
    dabled::LED_Status_Toggle();

    //
    // Acknowledge this interrupt to receive more interrupts from group 1
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
}

// disabled because not used

////
//// epwm1ISR - ePWM 1 ISR
////
//__interrupt void epwm1ISR(void)
//{
//    //
//    // Clear INT flag for this timer
//    //
//    EPWM_clearEventTriggerInterruptFlag(PWM_LLC1_1_BASE);
//
//    //
//    // Acknowledge interrupt group
//    //
//    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
//}
//
////
//// epwm2ISR - ePWM 2 ISR
////
//__interrupt void epwm2ISR(void)
//{
//    //
//    // Clear INT flag for this timer
//    //
//    EPWM_clearEventTriggerInterruptFlag(PWM_LLC1_2_BASE);
//
//    //
//    // Acknowledge interrupt group
//    //
//    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
//}
//
////
//// epwm3ISR - ePWM 3 ISR
////
//__interrupt void epwm3ISR(void)
//{
//    //
//    // Clear INT flag for this timer
//    //
//    EPWM_clearEventTriggerInterruptFlag(PWM_LLC2_1_BASE);
//
//    //
//    // Acknowledge interrupt group
//    //
//    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
//}
//
////
//// epwm4ISR - ePWM 4 ISR
////
//__interrupt void epwm4ISR(void)
//{
//    //
//    // Clear INT flag for this timer
//    //
//    EPWM_clearEventTriggerInterruptFlag(PWM_LLC2_2_BASE);
//
//    //
//    // Acknowledge interrupt group
//    //
//    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
//}

//
// End of File
//
