/*
 * dab_ser.h

    Author: strayedelectron
    Created on: 03.02.2023

    DAB Modulation Controller
    Copyright (C) 2023  strayedelectron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef DAB_SER_H_
#define DAB_SER_H_

// Some basic defines are in here
#include "dab.h"
// TI 28x driverlib project basics
#include "driverlib.h"
#include "device.h"
#include "board.h"
#include "c2000ware_libraries.h"
// Some C++ String libs
//#include <iostream> // Is too big for memory! Is too big for flash!
//#include <string> // Is too big for memory!
//#include <cstdio>

//
// Config
//
#define CHAR_BUFFER_LENGTH 32
#define DATA_PACKET_LENGTH 12

struct dab_params
{
    uint16_t state;
    int16_t phi;
    uint16_t tau1;
    uint16_t tau2;
    uint16_t deadtime1;
    uint16_t deadtime2;
};

//
// Function Prototypes
//
__interrupt void INT_SerialA_TX_ISR(void);
__interrupt void INT_SerialA_RX_ISR(void);

namespace dabser
{

void charbuf_append(char c);
void charbuf_reset(void);
bool charbuf_not_full(void);
bool handle_data_packet(struct dab_params *dab);

// Serial init
void init(void);

// Serial
void SendMessage(const char *msg);

// Implementation of missing functions in C++ 98
const char* to_string(int x);
//std::string to_string(int x);

//returns the size of a character array using a pointer to the first element of the character array
uint16_t size(const char *ptr);

uint16_t char_to_uint16(char hi, char low);
int16_t char_to_int16(char hi, char low);

}  // namespace dabser

#endif /* DAB_SER_H_ */
