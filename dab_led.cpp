/*
 * dab_led.cpp

    Author: strayedelectron
    Created on: 03.02.2023

    DAB Modulation Controller
    Copyright (C) 2023  strayedelectron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

#include "dab_led.h"

namespace dabled
{

void LED_Status_Toggle(void)
{
    GPIO_togglePin(LED_Status_GPIO);
}

void LED_Status_ON(void)
{
    GPIO_writePin(LED_Status_GPIO, LED_ON);
}

void LED_Status_OFF(void)
{
    GPIO_writePin(LED_Status_GPIO, LED_OFF);
}

void LED_Error_Toggle(void)
{
    GPIO_togglePin(LED_Error_GPIO);
}

void LED_Error_ON(void)
{
    GPIO_writePin(LED_Error_GPIO, LED_ON);
}

void LED_Error_OFF(void)
{
    GPIO_writePin(LED_Error_GPIO, LED_OFF);
}

}  // namespace dabled

