/*
 * dab_led.h

    Author: strayedelectron
    Created on: 03.02.2023

    DAB Modulation Controller (driverlib)
    Copyright (C) 2023  strayedelectron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef DAB_LED_H_
#define DAB_LED_H_

// Some basic defines are in here
#include "dab.h"
// TI 28x driverlib project basics
#include "driverlib.h"
#include "device.h"
#include "board.h"
#include "c2000ware_libraries.h"

namespace dabled
{

#ifdef _LAUNCHXL_F280049C
    #define LED_ON  0
    #define LED_OFF 1
#else
    #define LED_ON  1
    #define LED_OFF 0
#endif

void LED_Status_Toggle(void);
void LED_Status_ON(void);
void LED_Status_OFF(void);
void LED_Error_Toggle(void);
void LED_Error_ON(void);
void LED_Error_OFF(void);

}  // namespace dabled

#endif /* DAB_LED_H_ */
