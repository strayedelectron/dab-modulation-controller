/**
 * These arguments were used when this file was generated. They will be automatically applied on subsequent loads
 * via the GUI or CLI. Run CLI with '--help' for additional information on how to override these arguments.
 * @cliArgs --device "F28004x" --package "F28004x_100PZ" --part "F28004x_100PZ" --context "system" --product "C2000WARE@4.02.00.00"
 * @versions {"tool":"1.15.0+2826"}
 */

/**
 * Import the modules used in this configuration.
 */
const led       = scripting.addModule("/driverlib/board_components/led", {}, false);
const led1      = led.addInstance();
const led2      = led.addInstance();
const cputimer  = scripting.addModule("/driverlib/cputimer.js", {}, false);
const cputimer1 = cputimer.addInstance();
const epwm      = scripting.addModule("/driverlib/epwm.js", {}, false);
const epwm1     = epwm.addInstance();
const epwm2     = epwm.addInstance();
const epwm3     = epwm.addInstance();
const epwm4     = epwm.addInstance();
const gpio      = scripting.addModule("/driverlib/gpio.js", {}, false);
const gpio3     = gpio.addInstance();
const gpio4     = gpio.addInstance();
const gpio5     = gpio.addInstance();
const gpio6     = gpio.addInstance();
const gpio7     = gpio.addInstance();
const sci       = scripting.addModule("/driverlib/sci.js", {}, false);
const sci1      = sci.addInstance();
const sync      = scripting.addModule("/driverlib/sync.js");

/**
 * Write custom configuration values to the imported modules.
 */
led1.$name                  = "LED_Status";
led1.gpio.writeInitialValue = true;
led1.gpio.initialValue      = 1;
led1.gpio.gpioPin.$assign   = "GPIO34";

led2.$name                  = "LED_Error";
led2.gpio.writeInitialValue = true;
led2.gpio.initialValue      = 1;
led2.gpio.gpioPin.$assign   = "GPIO23_VSW";

cputimer1.$name                    = "myCPUTIMER0";
cputimer1.timerPrescaler           = 10000;
cputimer1.enableInterrupt          = true;
cputimer1.registerInterrupts       = true;
cputimer1.startTimer               = true;
cputimer1.timerPeriod              = 10000;
cputimer1.emulationMode            = "CPUTIMER_EMULATIONMODE_RUNFREE";
cputimer1.timerInt.enableInterrupt = true;

epwm1.$name                                                    = "PWM_LLC1_1";
epwm1.epwmTimebase_period                                      = 499;
epwm1.epwmTimebase_hsClockDiv                                  = "EPWM_HSCLOCK_DIVIDER_1";
epwm1.epwmTimebase_counterMode                                 = "EPWM_COUNTER_MODE_UP";
epwm1.epwmCounterCompare_cmpA                                  = 250;
epwm1.epwmActionQualifier_EPWM_AQ_OUTPUT_A_ON_TIMEBASE_ZERO    = "EPWM_AQ_OUTPUT_HIGH";
epwm1.epwmActionQualifier_EPWM_AQ_OUTPUT_A_ON_TIMEBASE_UP_CMPA = "EPWM_AQ_OUTPUT_LOW";
epwm1.epwmActionQualifier_EPWM_AQ_OUTPUT_B_ON_TIMEBASE_ZERO    = "EPWM_AQ_OUTPUT_LOW";
epwm1.epwmActionQualifier_EPWM_AQ_OUTPUT_B_ON_TIMEBASE_UP_CMPA = "EPWM_AQ_OUTPUT_HIGH";
epwm1.epwmTimebase_emulationMode                               = "EPWM_EMULATION_FREE_RUN";
epwm1.epwmDeadband_delayRED                                    = 5;
epwm1.epwmDeadband_delayFED                                    = 5;
epwm1.epwmDeadband_polarityFED                                 = "EPWM_DB_POLARITY_ACTIVE_LOW";
epwm1.epwmDeadband_enableRED                                   = true;
epwm1.epwmDeadband_enableFED                                   = true;
epwm1.epwmDeadband_fedShadowMode                               = true;
epwm1.epwmDeadband_redShadowMode                               = true;
epwm1.epwmDeadband_controlShadowMode                           = true;
epwm1.epwmTimebase_syncOutPulseMode                            = "EPWM_SYNC_OUT_PULSE_ON_COUNTER_COMPARE_B";
epwm1.epwmCounterCompare_cmpB                                  = 498;
epwm1.epwm.$assign                                             = "EPWM1";

epwm2.$name                                                    = "PWM_LLC1_2";
epwm2.copyUse                                                  = true;
epwm2.copyFrom                                                 = "PWM_LLC1_1";
epwm2.epwmTimebase_hsClockDiv                                  = "EPWM_HSCLOCK_DIVIDER_1";
epwm2.epwmTimebase_phaseEnable                                 = true;
epwm2.epwmTimebase_period                                      = 499;
epwm2.epwmCounterCompare_cmpA                                  = 250;
epwm2.epwmTimebase_syncOutPulseMode                            = "EPWM_SYNC_OUT_PULSE_ON_EPWMxSYNCIN";
epwm2.epwmTimebase_counterMode                                 = "EPWM_COUNTER_MODE_UP";
epwm2.epwmActionQualifier_EPWM_AQ_OUTPUT_A_ON_TIMEBASE_ZERO    = "EPWM_AQ_OUTPUT_HIGH";
epwm2.epwmActionQualifier_EPWM_AQ_OUTPUT_A_ON_TIMEBASE_UP_CMPA = "EPWM_AQ_OUTPUT_LOW";
epwm2.epwmActionQualifier_EPWM_AQ_OUTPUT_B_ON_TIMEBASE_ZERO    = "EPWM_AQ_OUTPUT_LOW";
epwm2.epwmActionQualifier_EPWM_AQ_OUTPUT_B_ON_TIMEBASE_UP_CMPA = "EPWM_AQ_OUTPUT_HIGH";
epwm2.epwmTimebase_emulationMode                               = "EPWM_EMULATION_FREE_RUN";
epwm2.epwmDeadband_polarityFED                                 = "EPWM_DB_POLARITY_ACTIVE_LOW";
epwm2.epwmDeadband_enableRED                                   = true;
epwm2.epwmDeadband_delayRED                                    = 5;
epwm2.epwmDeadband_enableFED                                   = true;
epwm2.epwmDeadband_delayFED                                    = 5;
epwm2.epwmDeadband_controlShadowMode                           = true;
epwm2.epwmDeadband_redShadowMode                               = true;
epwm2.epwmDeadband_fedShadowMode                               = true;
epwm2.epwmCounterCompare_cmpALink                              = "EPWM_LINK_WITH_EPWM_1";
epwm2.epwm.$assign                                             = "EPWM2";

epwm3.$name                                                    = "PWM_LLC2_1";
epwm3.epwmTimebase_hsClockDiv                                  = "EPWM_HSCLOCK_DIVIDER_1";
epwm3.epwmTimebase_period                                      = 499;
epwm3.epwmTimebase_counterMode                                 = "EPWM_COUNTER_MODE_UP";
epwm3.epwmTimebase_phaseEnable                                 = true;
epwm3.epwmCounterCompare_cmpA                                  = 250;
epwm3.epwmActionQualifier_EPWM_AQ_OUTPUT_A_ON_TIMEBASE_ZERO    = "EPWM_AQ_OUTPUT_HIGH";
epwm3.epwmActionQualifier_EPWM_AQ_OUTPUT_A_ON_TIMEBASE_UP_CMPA = "EPWM_AQ_OUTPUT_LOW";
epwm3.epwmActionQualifier_EPWM_AQ_OUTPUT_B_ON_TIMEBASE_ZERO    = "EPWM_AQ_OUTPUT_LOW";
epwm3.epwmActionQualifier_EPWM_AQ_OUTPUT_B_ON_TIMEBASE_UP_CMPA = "EPWM_AQ_OUTPUT_HIGH";
epwm3.epwmTimebase_emulationMode                               = "EPWM_EMULATION_FREE_RUN";
epwm3.epwmDeadband_polarityFED                                 = "EPWM_DB_POLARITY_ACTIVE_LOW";
epwm3.epwmDeadband_enableRED                                   = true;
epwm3.epwmDeadband_delayRED                                    = 5;
epwm3.epwmDeadband_enableFED                                   = true;
epwm3.epwmDeadband_delayFED                                    = 5;
epwm3.epwmDeadband_controlShadowMode                           = true;
epwm3.epwmDeadband_redShadowMode                               = true;
epwm3.epwmDeadband_fedShadowMode                               = true;
epwm3.epwmTimebase_syncOutPulseMode                            = "EPWM_SYNC_OUT_PULSE_ON_EPWMxSYNCIN";
epwm3.epwmCounterCompare_cmpALink                              = "EPWM_LINK_WITH_EPWM_1";
epwm3.epwm.$assign                                             = "EPWM3";

epwm4.$name                                                    = "PWM_LLC2_2";
epwm4.epwmTimebase_hsClockDiv                                  = "EPWM_HSCLOCK_DIVIDER_1";
epwm4.epwmTimebase_period                                      = 499;
epwm4.epwmTimebase_counterMode                                 = "EPWM_COUNTER_MODE_UP";
epwm4.epwmTimebase_phaseEnable                                 = true;
epwm4.epwmCounterCompare_cmpA                                  = 250;
epwm4.epwmActionQualifier_EPWM_AQ_OUTPUT_A_ON_TIMEBASE_ZERO    = "EPWM_AQ_OUTPUT_HIGH";
epwm4.epwmActionQualifier_EPWM_AQ_OUTPUT_A_ON_TIMEBASE_UP_CMPA = "EPWM_AQ_OUTPUT_LOW";
epwm4.epwmActionQualifier_EPWM_AQ_OUTPUT_B_ON_TIMEBASE_ZERO    = "EPWM_AQ_OUTPUT_LOW";
epwm4.epwmActionQualifier_EPWM_AQ_OUTPUT_B_ON_TIMEBASE_UP_CMPA = "EPWM_AQ_OUTPUT_HIGH";
epwm4.epwmTimebase_syncOutPulseMode                            = "EPWM_SYNC_OUT_PULSE_ON_EPWMxSYNCIN";
epwm4.epwmTimebase_emulationMode                               = "EPWM_EMULATION_FREE_RUN";
epwm4.epwmDeadband_polarityFED                                 = "EPWM_DB_POLARITY_ACTIVE_LOW";
epwm4.epwmDeadband_enableRED                                   = true;
epwm4.epwmDeadband_delayRED                                    = 5;
epwm4.epwmDeadband_enableFED                                   = true;
epwm4.epwmDeadband_delayFED                                    = 5;
epwm4.epwmDeadband_fedShadowMode                               = true;
epwm4.epwmDeadband_redShadowMode                               = true;
epwm4.epwmDeadband_controlShadowMode                           = true;
epwm4.epwmCounterCompare_cmpALink                              = "EPWM_LINK_WITH_EPWM_1";
epwm4.epwm.$assign                                             = "EPWM4";

gpio3.$name             = "DO_EN_LLC1";
gpio3.direction         = "GPIO_DIR_MODE_OUT";
gpio3.writeInitialValue = true;
gpio3.initialValue      = 1;
gpio3.gpioPin.$assign   = "GPIO56";

gpio4.$name             = "DO_EN_LLC2";
gpio4.direction         = "GPIO_DIR_MODE_OUT";
gpio4.writeInitialValue = true;
gpio4.initialValue      = 1;
gpio4.gpioPin.$assign   = "GPIO57";

gpio5.$name             = "DO_nLatch_Enable";
gpio5.direction         = "GPIO_DIR_MODE_OUT";
gpio5.writeInitialValue = true;
gpio5.gpioPin.$assign   = "GPIO31";

gpio6.$name             = "DO_HwLatch_RESET";
gpio6.direction         = "GPIO_DIR_MODE_OUT";
gpio6.writeInitialValue = true;
gpio6.gpioPin.$assign   = "GPIO33";

gpio7.$name           = "DI_nHwErrLatch";
gpio7.padConfig       = "PULLUP";
gpio7.gpioPin.$assign = "GPIO40";

sci1.$name                 = "SerialA";
sci1.registerInterrupts    = true;
sci1.sci.$assign           = "SCIA";
sci1.sci.sci_rxPin.$assign = "GPIO28";
sci1.sci.sci_txPin.$assign = "GPIO29";

/**
 * Pinmux solution for unlocked pins/peripherals. This ensures that minor changes to the automatic solver in a future
 * version of the tool will not impact the pinmux you originally saw.  These lines can be completely deleted in order to
 * re-solve from scratch.
 */
epwm1.epwm.epwm_aPin.$suggestSolution = "GPIO0";
epwm1.epwm.epwm_bPin.$suggestSolution = "GPIO1";
epwm2.epwm.epwm_aPin.$suggestSolution = "GPIO2";
epwm2.epwm.epwm_bPin.$suggestSolution = "GPIO3";
epwm3.epwm.epwm_aPin.$suggestSolution = "GPIO4";
epwm3.epwm.epwm_bPin.$suggestSolution = "GPIO5";
epwm4.epwm.epwm_aPin.$suggestSolution = "GPIO6";
epwm4.epwm.epwm_bPin.$suggestSolution = "GPIO7";
