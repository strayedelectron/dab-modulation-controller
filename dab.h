/*
 * dab.h

    Author: strayedelectron
    Created on: 03.02.2023

    DAB Modulation Controller (driverlib)
    Copyright (C) 2023  strayedelectron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef DAB_H_
#define DAB_H_

// DAB Project includes
#include "dab_led.h"
#include "dab_pwm.h"
#include "dab_ser.h"
// TI 28x driverlib project basics
#include "driverlib.h"
#include "device.h"
#include "board.h"
#include "c2000ware_libraries.h"

// CPU Freq: device.h defines DEVICE_SYSCLK_FREQ
// ePWM Clock divider (total)
#define EPWM_CLKDIV             1U
// EPWM clock period duration in nano-seconds
#define EPWM_CLKPERIOD_NS       (1000000000UL / (DEVICE_SYSCLK_FREQ / EPWM_CLKDIV))
// EPWM DeadBand Counter Clock Cycle (normal 1, HR 2)
#define EPWM_DB_CLK             1

// ePWM Interrupt routines
// disabled because not used
//__interrupt void epwm1ISR(void);
//__interrupt void epwm2ISR(void);
//__interrupt void epwm3ISR(void);
//__interrupt void epwm4ISR(void);

//
// Main
//
void main(void);

#endif /* DAB_H_ */
