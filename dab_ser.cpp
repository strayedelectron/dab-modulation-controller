/*
 * dab_ser.cpp

    Author: strayedelectron
    Created on: 03.02.2023

    DAB Modulation Controller
    Copyright (C) 2023  strayedelectron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

#include "dab_ser.h"

#include "driverlib.h"
#include "device.h"

//
// Serial TX interrupt handler
//
__interrupt void INT_SerialA_TX_ISR(void)
{

    //
    // Acknowledge the PIE interrupt.
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);
}

//
// Serial RX interrupt handler
//
__interrupt void INT_SerialA_RX_ISR(void)
{

    const char *msg;
    uint16_t receivedChar1;

    //
    // Enable the TXFF interrupt again.
    //
    SCI_enableInterrupt(SerialA_BASE, SCI_INT_TXFF);

    //
    // Read two characters from the FIFO.
    //
    receivedChar1 = SCI_readCharBlockingFIFO(SerialA_BASE);

    //
    // Echo back the two characters.
    //
    msg = "\nYou sent: ";
    SCI_writeCharArray(SerialA_BASE, (uint16_t*) msg, 11);
    SCI_writeCharBlockingFIFO(SerialA_BASE, receivedChar1);

    //
    // Clear the SCI RXFF interrupt and acknowledge the PIE interrupt.
    //
    SCI_clearInterruptStatus(SerialA_BASE, SCI_INT_RXFF);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);

    dabled::LED_Error_Toggle();
}

namespace dabser
{

static char charbuf[CHAR_BUFFER_LENGTH];
uint16_t charbuf_i;

// ----------------- CHAR BUFFER HELPERS -------------------

/** Add a byte to charbuf, error on overflow */
void charbuf_append(char c)
{
    if (charbuf_i >= CHAR_BUFFER_LENGTH)
    {
        SendMessage("ERR: RX Buffer overflow!\n");
    }

    charbuf[charbuf_i++] = c;
}

/** Terminate charbuf and rewind the pointer to start */
void charbuf_reset(void)
{
//    charbuf[charbuf_i] = '\0';
    charbuf_i = 0;
}

/** Check if charbuf is not full */
bool charbuf_not_full(void)
{
    return charbuf_i < CHAR_BUFFER_LENGTH;
}

/**
 * Check if data packet is complete and parse it.
 * Will return true if a data packet was received.
 * */
bool handle_data_packet(struct dab_params *dab)
{
    if (charbuf_i == DATA_PACKET_LENGTH)
    {
        // Return the received dab params for the sender to double check
        SCI_writeCharArray(SerialA_BASE, (uint16_t*) charbuf, charbuf_i);
        // Static data disassembly
        // A full packet may look like this in hex:
        // 00AA00001338133800320032
        dab->state = char_to_uint16(charbuf[0], charbuf[1]);
        dab->phi = char_to_uint16(charbuf[2], charbuf[3]);
        dab->tau1 = char_to_uint16(charbuf[4], charbuf[5]);
        dab->tau2 = char_to_uint16(charbuf[6], charbuf[7]);
        dab->deadtime1 = char_to_uint16(charbuf[8], charbuf[9]);
        dab->deadtime2 = char_to_uint16(charbuf[10], charbuf[11]);
        charbuf_reset();
        SendMessage("OK: RX Data Packet\n");
        return true;
    }
    return false;
}


//
// Initialize the Serial Port
//
void init(void)
{
    // Enable the RX/TX FIFO Level Interrupt
//    SCI_enableInterrupt(SerialA_BASE, SCI_INT_TXFF | SCI_INT_RXFF);
//    SCI_enableInterrupt(SerialA_BASE, SCI_INT_RXFF);
}

//
// Send a message oder Serial.
//
void SendMessage(const char *msg)
{
//    uint16_t len = strlen(msg);
    uint16_t len = size(msg);
    SCI_writeCharArray(SerialA_BASE, (uint16_t*) msg, len);
}

// Implementation of missing functions in C++ 98

//
// Convert an integer to string
//
// DUMMY to save space without cstdio
const char* to_string(int x)
{
    const char *buf = "FAIL: to_string not implemented!";
    return buf;
}

//char* to_string(int x)
//{
//    int length = snprintf((char*) NULL, 0, "%d", x);
////    assert(length >= 0);
//    char *buf = new char[length + 1];
//    snprintf(buf, length + 1, "%d", x);
//    return buf;
//}
// You can do more with it. Just use "%g" to convert float or double to string, use "%x" to convert int to hex representation, and so on.

//std::string to_string(int x)
//{
//    int length = snprintf((char *)NULL, 0, "%d", x);
////    assert(length >= 0);
//    char *buf = new char[length + 1];
//    snprintf(buf, length + 1, "%d", x);
//    std::string str(buf);
//    delete[] buf;
//    return str;
//}

//
// Returns the size of a character array
// using a pointer to the first element of the character array
//
uint16_t size(const char *ptr)
{
    //variable used to access the subsequent array elements.
    int offset = 0;
    //variable that counts the number of elements in your array
    int count = 0;

    //While loop that tests whether the end of the array has been reached
    while (*(ptr + offset) != '\0')
    {
        //increment the count variable
        ++count;
        //advance to the next element of the array
        ++offset;
    }
    //return the size of the array
    return count;
}

//
// Get integer out of two chars
//
uint16_t char_to_uint16(char hi, char low)
{
    return (uint16_t(hi) << 8) | low;
}

int16_t char_to_int16(char hi, char low)
{
    return (int16_t(hi) << 8) | low;
}

}    // namespace dabser

