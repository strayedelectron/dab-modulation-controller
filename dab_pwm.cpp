/*
 * dab_pwm.cpp

    Author: strayedelectron
    Created on: 03.02.2023

    DAB Modulation Controller
    Copyright (C) 2023  strayedelectron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

#include "dab_pwm.h"

namespace dabmod
{

//*****************************************************************************
//
// Set all PWM DAB Phase shifts
//
// The duty cycle is multiplied by 10000, e.g.: 0.25 would be 2500
// Duty cycle = 1 equals to 360 degree or 2Pi rad or TimeBase eg. 5µs
// ex.: 2500 eq. 0.25 eq. 90 deg. eq. Pi/2 eq. 5µs/4
// Maximum useful value would be 5000 but max 10000 is allowed [0, 10000].
// For phi also negative values are allowed [-10000, 10000].
// New values will be active in the next ePWM cycle.
//
//*****************************************************************************
void SetDabPhaseShifts(int16_t phi_dutycycle, uint16_t tau1_dutycycle, uint16_t tau2_dutycycle)
{
    // Check duty cycle limits
    // TODO raise some error if exceeded or limit to min/max, for now set to zero?
    if (phi_dutycycle < -10000 or phi_dutycycle > 10000)
        phi_dutycycle = 0;
    if (tau1_dutycycle > 10000)
        tau1_dutycycle = 0;
    if (tau2_dutycycle > 10000)
        tau2_dutycycle = 0;

//    uint16_t pwm1_hb11_phase = 0; // ePWM1 phase is always zero
    uint32_t tau1_pwm2_hb12_phase = 0;
    uint32_t phi_pwm3_hb21_phase = 0;
    uint32_t tau2_pwm4_hb22_phase = 0;

    // get the TimeBase/2 which equals to CMPA register, assuming that we have 50% duty cycle PWM
    // CMPA and TimeBase _MUST_ be the same for all ePWMs!
    // Set type to uint32_t in order to get sane results on following multiplications
    uint32_t pwm_tb_counts = 2 * EPWM_getCounterCompareValue(PWM_LLC1_1_BASE, EPWM_COUNTER_COMPARE_A);

    // Calculate the phases:

    // First step with 10ps resolution
    // Time Base
    uint32_t pwm_tb_max = pwm_tb_counts * 10000;

    // tau1
    tau1_pwm2_hb12_phase = pwm_tb_counts * tau1_dutycycle;

    // phi
    phi_pwm3_hb21_phase = pwm_tb_max - pwm_tb_counts * phi_dutycycle;
    // phi can have 1*360° phase offset
    if (phi_pwm3_hb21_phase >= pwm_tb_max)
        phi_pwm3_hb21_phase -= pwm_tb_max;

    // tau2
    tau2_pwm4_hb22_phase = pwm_tb_max + pwm_tb_counts * tau2_dutycycle + phi_pwm3_hb21_phase;
    // tau2 can have 2*360° phase offset
    if (tau2_pwm4_hb22_phase >= pwm_tb_max)
        tau2_pwm4_hb22_phase -= pwm_tb_max;
    if (tau2_pwm4_hb22_phase >= pwm_tb_max)
        tau2_pwm4_hb22_phase -= pwm_tb_max;

    // Second step round to 10ns resolution
    // as this is the minimum TBCLK resolution at 100MHz
    // Integer div will floor the result, we therefore loose resolution
    tau1_pwm2_hb12_phase = tau1_pwm2_hb12_phase / 10000;
    phi_pwm3_hb21_phase = phi_pwm3_hb21_phase / 10000;
    tau2_pwm4_hb22_phase = tau2_pwm4_hb22_phase / 10000;
    // TODO use the modulo for HRPWM to calc the MEP steps separately

    // set the new phase values
    EPWM_setPhaseShift(PWM_LLC1_2_BASE, (uint16_t) tau1_pwm2_hb12_phase);
    EPWM_setPhaseShift(PWM_LLC2_1_BASE, (uint16_t) phi_pwm3_hb21_phase);
    EPWM_setPhaseShift(PWM_LLC2_2_BASE, (uint16_t) tau2_pwm4_hb22_phase);
}

// Set PWM rising edge dead time in nano-seconds
void SetDabDeadTime(uint32_t deadtime_ns)
{
    uint16_t deadtime_counts = deadtime_ns / EPWM_CLKPERIOD_NS * EPWM_DB_CLK;
    EPWM_setRisingEdgeDelayCount(PWM_LLC1_1_BASE, deadtime_counts);
    EPWM_setFallingEdgeDelayCount(PWM_LLC1_1_BASE, deadtime_counts);
    EPWM_setRisingEdgeDelayCount(PWM_LLC1_2_BASE, deadtime_counts);
    EPWM_setFallingEdgeDelayCount(PWM_LLC1_2_BASE, deadtime_counts);
    EPWM_setRisingEdgeDelayCount(PWM_LLC2_1_BASE, deadtime_counts);
    EPWM_setFallingEdgeDelayCount(PWM_LLC2_1_BASE, deadtime_counts);
    EPWM_setRisingEdgeDelayCount(PWM_LLC2_2_BASE, deadtime_counts);
    EPWM_setFallingEdgeDelayCount(PWM_LLC2_2_BASE, deadtime_counts);

//    EPWM_setDeadBandDelayMode(PWM_LLC2_2_BASE, EPWM_DB_RED, true);
//    EPWM_setDeadBandDelayMode(PWM_LLC2_2_BASE, EPWM_DB_FED, true);
}

// Set initial phase shift and dead time to almost zero power transfer
void InitDABsanePhaseDead(void)
{
    //
    // DAB PWM initial configuration
    //
    // DAB PWM deadtime for init
    uint16_t deadtime_ns = 50;
    dabmod::SetDabDeadTime(deadtime_ns);
    // DAB Phase angles/duty cycles init defaults
    int16_t phi_dutycycle = 0;
    uint16_t tau1_dutycycle = 5000;
    uint16_t tau2_dutycycle = 5000;
    dabmod::SetDabPhaseShifts(phi_dutycycle, tau1_dutycycle, tau2_dutycycle);
}



// Start/Stop PWM Timers

// Start Timer counting
void TimersEnable(void)
{
    EPWM_setTimeBaseCounterMode(PWM_LLC1_1_BASE, EPWM_COUNTER_MODE_UP);
    EPWM_setTimeBaseCounterMode(PWM_LLC1_2_BASE, EPWM_COUNTER_MODE_UP);
    EPWM_setTimeBaseCounterMode(PWM_LLC2_1_BASE, EPWM_COUNTER_MODE_UP);
    EPWM_setTimeBaseCounterMode(PWM_LLC2_2_BASE, EPWM_COUNTER_MODE_UP);
}

// Stop Timer counting
void TimersDisable(void)
{
    EPWM_setTimeBaseCounterMode(PWM_LLC1_1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
    EPWM_setTimeBaseCounterMode(PWM_LLC1_2_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
    EPWM_setTimeBaseCounterMode(PWM_LLC2_1_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
    EPWM_setTimeBaseCounterMode(PWM_LLC2_2_BASE, EPWM_COUNTER_MODE_STOP_FREEZE);
}

// Enable/Disable Bridge Gate drivers

// Enables the Bridge Gate drivers
void BridgeOnLatch(void)
{
    GPIO_writePin(DO_nLatch_Enable, 1);
    HwLatchReset();
    // This can enable/disable the bridges directly
    // Due to HW errors only bridge 2 can be disabled!
    // TODO remove this
    GPIO_writePin(DO_EN_LLC1, 1);
    GPIO_writePin(DO_EN_LLC2, 1);
}

// Disable the Bridge Gate drivers
void BridgeOffLatch(void)
{
    // This will trigger the HW Latch and Bridge stays off until reset
    GPIO_writePin(DO_nLatch_Enable, 0);
    // This can enable/disable the bridges directly
    // Due to HW errors only bridge 2 can be disabled!
    // TODO remove this
    GPIO_writePin(DO_EN_LLC1, 0);
    GPIO_writePin(DO_EN_LLC2, 0);
}
// Latch status and reset

// Returns the status of the HW Error Latch
bool HwErrLatch(void)
{
    return GPIO_readPin(DI_nHwErrLatch) == 0;
}

// Reset the HW Error Latch
void HwLatchReset(void)
{
    GPIO_writePin(DO_HwLatch_RESET, 1);
    DEVICE_DELAY_US(1000);
    GPIO_writePin(DO_HwLatch_RESET, 0);
}

//*****************************************************************************
//
// Error handling function to stop the DAB and halt the program
//
//*****************************************************************************
void Error(const char *filename, uint32_t line)
{
    // Stop the DAB
    // TODO EPWM Trigger set outputs to low
    dabmod::BridgeOffLatch();
    //
    // An ASSERT condition was evaluated as false. You can use the filename and
    // line parameters to determine what went wrong.
    //
    __error__(filename, line);
}

}  // namespace dabmod

